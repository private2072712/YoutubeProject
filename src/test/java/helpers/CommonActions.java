package helpers;

import java.util.Random;

public class CommonActions {
    public static String generateRandomNumberFrom10To9999() {
        int min = 10;
        int max = 9999;
        Random random = new Random();
        int randomNumber = random.nextInt(max - min) + min;

        return Integer.toString(randomNumber);
    }

    public static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
