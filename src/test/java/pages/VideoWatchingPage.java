package pages;

import org.openqa.selenium.WebDriver;

public class VideoWatchingPage extends BasePage{
    private static final String OWNER_AVATAR_ICON = "//div[@id='owner']//img";

    public VideoWatchingPage(WebDriver driver) {
        super(driver);
    }

    public VideoWatchingPage clickOnVideoOwnerAvatarIcon() {
        findElementByXpathWithWaitingToBecomeClickable(OWNER_AVATAR_ICON).click();
        return this;
    }

    public MainChannelsPage goToMainChannelsPage(WebDriver driver) {
        return new MainChannelsPage(driver);
    }
}
