package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static helpers.CommonActions.sleep;

public class HomePage extends BasePage {

    private static final String SEARCH_FIELD = "//input[@id='search']";
    private static final String SEARCH_BTN = "//button[@id='search-icon-legacy']";

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public boolean isTittleMatches(String str) {
        return driver.getTitle().equals(str);
    }

    public HomePage inputTextToSearch(String str) {
        findElementByXpathWithWaitingToBecomeClickable(SEARCH_FIELD).sendKeys(str);
        findElementByXpathWithWaitingToBecomeClickable(SEARCH_FIELD).sendKeys(Keys.DOWN);
        sleep(1);
        findElementByXpathWithWaitingToBecomeClickable(SEARCH_FIELD).sendKeys(Keys.ARROW_DOWN, Keys.ARROW_DOWN + "\n");
        return this;
    }

    public VideoResultFeedPage goToVideoResultFeedPage(WebDriver driver){
        return new VideoResultFeedPage(driver);
    }
}
