package pages;

import org.openqa.selenium.WebDriver;

public class VideoResultFeedPage extends BasePage{
    private static final String VIDEO_BY_SERIAL_NUMBER = "(//a[@id='video-title'])[%s]";

    public VideoResultFeedPage(WebDriver driver) {
        super(driver);
    }
    public VideoResultFeedPage openVideoBySerialNumber(String i) {
        findElementByXpathWithWaitingToBecomeClickable(String.format(VIDEO_BY_SERIAL_NUMBER, i)).click();
        return this;
    }

    public VideoWatchingPage goToVideoWatchingPage(WebDriver driver) {
        return new VideoWatchingPage(driver);
    }
}
