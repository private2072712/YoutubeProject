package pages;

import org.openqa.selenium.WebDriver;

public class MainChannelsPage extends BasePage{
    private static final String SUBSCRIBE_BTN = "//div[@id='inner-header-container']//button";
    private static final String SIGN_IN_BTN = "(//div[@id='contentWrapper'])[3]//ytd-button-renderer//span";

    public MainChannelsPage(WebDriver driver) {
        super(driver);
    }

    public MainChannelsPage clickSubscribeBtn() {
        findElementByXpathWithWaitingToBecomeClickable(SUBSCRIBE_BTN).click();
        return this;
    }

    public String getSignInBtnText() {
        return findElementByXpathWithWaitingToBecomeClickable(SIGN_IN_BTN).getText();
    }
}
