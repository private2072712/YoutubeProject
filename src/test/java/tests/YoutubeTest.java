package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.MainChannelsPage;
import pages.VideoResultFeedPage;
import pages.VideoWatchingPage;

import static helpers.CommonActions.generateRandomNumberFrom10To9999;
import static helpers.CommonActions.sleep;

public class YoutubeTest extends BaseTest{

    @Test
    public void checkSubscriptionWithUnauthorizedUser(){
        openUrl(baseUrl);
        HomePage homePage = new HomePage(driver);

        Assert.assertTrue(homePage.isTittleMatches("YouTube"));
        homePage.inputTextToSearch(generateRandomNumberFrom10To9999());

        VideoResultFeedPage videoResultFeedPage = homePage.goToVideoResultFeedPage(driver);
        videoResultFeedPage.openVideoBySerialNumber("4");

        VideoWatchingPage videoWatchingPage = videoResultFeedPage.goToVideoWatchingPage(driver);
        videoWatchingPage.clickOnVideoOwnerAvatarIcon();

        MainChannelsPage mainChannelsPage = videoWatchingPage.goToMainChannelsPage(driver);
        mainChannelsPage.clickSubscribeBtn();

        Assert.assertEquals(mainChannelsPage.getSignInBtnText(), "Увійти");
    }
}
